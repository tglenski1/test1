FROM ubuntu:latest

RUN apt-get update \
    && apt-get install -y python3-pip python3-dev \
    && ln -s /usr/bin/python3 python \
    && pip3 install --upgrade pip


ADD answer.py /


CMD [ "python3", "./answer.py" ]






